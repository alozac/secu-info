Projet de L3, réalisé dans le cadre d'une UE de sécurité informatique.
Ce projet implémente le chiffrement et déchiffrement de textes avec les chiffres de César, Vigénère et par permutations
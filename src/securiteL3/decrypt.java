package securiteL3;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class decrypt {
	
	public static void main(String[]args){
		if (args.length < 2) {
			System.err.println("Nombre d'arguments insuffisant!");
			return;
		}
		
		try {
			BufferedReader br = new BufferedReader(
									new FileReader(
										new File(args[1])));

			StringBuilder toDecrypt = new StringBuilder();
			String tmp = "";	

			while(( tmp = br.readLine()) != null)
				toDecrypt.append(tmp).append("\n");
			br.close();
			
			Chiffrement.initDico("lexique.php");
			
			long start = System.currentTimeMillis();
			if(args[0].equals("v")){
				if (args.length >= 3)
						Vigenere.decrypt(toDecrypt, Integer.parseInt(args[2]));
				else {
					System.err.println("Veuillez renseigner la taille du mot cle");
					return;
				}
			} else if(args[0].equals("c")){
				if (args.length >= 3){
					if (args[2].equals("1")){
						if (args.length >= 4)
							Cesar.decryptWord(toDecrypt, args[3]);
						else {
							System.err.println("Veuillez renseigner le mot connu");
							return;
						}
					} else if (args[2].equals("2"))
						Cesar.decryptFreq(toDecrypt);
					else if (args[2].equals("3"))
						Cesar.decryptBrute(toDecrypt);
					else {
						System.err.println("Strategie incorrecte!");
						return;
					}
				} else {
					System.err.println("Nombre d'arguments insuffisant!");
					return;
				}
			} else if(args[0].equals("p"))
				Permutation.decrypt(toDecrypt);
			else {
				System.err.println("Type de codage incorrect!");
				return;
			}
			long end = System.currentTimeMillis();
			System.err.println("Temps de " + (end - start) + "ms");
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		} catch (NumberFormatException e){
			e.printStackTrace();
		}
	}
	
}

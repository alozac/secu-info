package securiteL3;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class Permutation implements Chiffrement{
	
	char[] key     = new char[26];
	char[] dec_key = new char[26];
	
	static HashSet<Character> found = new HashSet<Character>();
	static HashSet<Character> foundTmp = new HashSet<Character>();
	static int inDico = 0;
	
	public Permutation(String str) throws IncorrectKeyException{
		str = str.toLowerCase();
		char c;
		for (int i = 0 ; i < str.length() ; i++){
			c = str.charAt(i);
			key[i] = c;
			dec_key[c - 'a'] = (char)(i + 'a');
		}
		for (int i = str.length() ; i < 26 ; i++){
			key[i] = (char)(i + 'a');
			dec_key[i] = (char)(i + 'a');
		}

	}
	
	public Permutation(char[] key){
		this.key = key;
		for (int i = 0 ; i < key.length ; i++)
			dec_key[key[i] - 'a'] = (char)(i + 'a');
		
		//for (int i = 0 ; i< 26;i++)System.out.print(dec_key[i]);
	}
	

	@Override
	public void chiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder s = new StringBuilder();
		char c;
		for (int i = 0; i < str.length(); i++){
			c = str.charAt(i);
			s.append( (c >= 'a' && c <= 'z') ? key[c - 'a']
										: c);
		}
		System.out.println(s);
	}

	@Override
	public void dechiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder s = new StringBuilder();
		char c;
		for (int i = 0 ; i < str.length(); i++){
			c = str.charAt(i);
			s.append( (c >= 'a' && c <= 'z') ? dec_key[c - 'a']
										: c);
		}
		System.out.println(s);
	}
	
	
	
	
	// key :           JBUECVAGWNHXDKYLMZFPOQTRIS
	// dec_key :       bwzpfrtseqomkucalhnjgyvxid;
		public static void decrypt(Object obj) {
			StringBuilder toDecrypt = StringFormalizer.formalize(obj);
			replaceLetByFreq(toDecrypt);
			found.add('e');
			found.add('a');
			toDecrypt = new StringBuilder(findLetDico(toDecrypt));
			System.out.println(toDecrypt);
		}
		
		private static String findLetDico(StringBuilder toDecrypt){
			String toDec = toDecrypt.toString();
			String tmp = "";
			String[] toDecWords = toDec.split("[^a-z]");
			inDico = countWordsInDico(toDecWords);
			for (String word : toDecWords){
			//	found.addAll(foundTmp);
				foundTmp.clear();
				if (partiallyDecrypted(word)){
					for (String dicWord : dico){
						if (wordMatches(word, dicWord)){
							if((tmp = replaceLetFromWord(toDec, word, dicWord)) != null){
								toDec = tmp;
							}
						}
					}
				}
			}
			System.out.println(inDico+"/"+toDecWords.length + 
								  " ( "+ inDico*100/toDecWords.length+"% )");
			return toDec;
		}
		
		private static void updateFound(String[] toDecryptWords){
			int[] nbGoodPlacements = new int[26];
			
			for (String word : toDecryptWords){
				if (dico.contains(word)){
					for (char c : word.toCharArray()){
						nbGoodPlacements[c - 'a']++;
					}
				}
			}
			
			for (int i = 0 ; i < 26 ; i++){
				System.out.print(nbGoodPlacements[i] + ", ");
			}
			System.out.println();
			char c;
			do {
				c = (char)(getMax(nbGoodPlacements) + 'a');
			} while (found.contains(c));
			System.out.println(c);
			found.add(c);
			
			
		}
		
		private static String replaceLetFromWord(String toDecrypt, String word, String dicWord) {
			String newToDec = toDecrypt;
			String tmp = "";
			HashSet<Character> foundTmpTmp = new HashSet<Character>();
			for (int i = 0 ; i < dicWord.length() ; i++){
				if (!found.contains(dicWord.charAt(i))){
					tmp      = newToDec.replace(dicWord.charAt(i), '#');
					newToDec = tmp.replace(word.charAt(i), dicWord.charAt(i));
					newToDec = newToDec.replace('#', word.charAt(i));
					foundTmpTmp.add(dicWord.charAt(i));
				}
			}
			int newInDico = countWordsInDico(newToDec.split("[^a-z]"));
		//	System.out.println(inDico+"   "+newInDico);
			if (newInDico > inDico){
				inDico = newInDico;
				foundTmp = foundTmpTmp;
				return newToDec;
			}
			return null;
		}

		private static boolean wordMatches(String word, String dicWord) {
			HashMap<Character, Character> matchChar = new HashMap<Character, Character>();
			int len = word.length();
			if (dicWord.length() != len)
				return false;
			char w, wDic;
			Character tmp;
			for (int i = 0 ; i < len ; i++){
				w    = word.charAt(i);
				wDic = dicWord.charAt(i);
				tmp  = matchChar.get(w);
				if ( (found.contains(w)  &&  wDic != w )
					|| (tmp != null && tmp != wDic) )
					return false;
				matchChar.put(w, wDic);
			}
			return true;
		}

		private static boolean partiallyDecrypted(String word) {
			int nbFound = 0;
			int len = word.length();
	//		if (len == 1) return false;
			for (int i = 0 ; i < len ; i++){
				if(found.contains(word.charAt(i)))
					nbFound++;
			}
			if (nbFound >= len/2 && nbFound != len)
				return true;
			return false;
		}
		
		private static int countWordsInDico(String[] toDecrypt){
			int inDico = 0;
			for (String word : toDecrypt){
				if (dico.contains(word) && word.length() > 1) inDico++;
			}
			return inDico;
		}
	
		
		private static Character replaceA(StringBuilder toDecrypt){
			String toDecryptStr = toDecrypt.toString();
			Map<Character, Integer> wordsOneLet = new HashMap<Character, Integer>();
			Integer tmp;
			for ( String word : toDecryptStr.split(" ")){
				if (word.length() == 1){
					wordsOneLet.put(word.charAt(0), ((tmp = wordsOneLet.get(word.charAt(0))) == null ? 0 : tmp) + 1 );
				}
			}
			Character max = Collections.max(wordsOneLet.entrySet(), Map.Entry.comparingByValue()).getKey();
			return max;
		}
		
		
		private static void replaceLetByFreq(StringBuilder toDecrypt){
			Integer[] freqText = new Integer[26];
			for (int i = 0 ; i < 26 ; i++)
				freqText[i] = 0;
			char c;
			for (int i = 0 ; i < toDecrypt.length() ; i++){
				c = toDecrypt.charAt(i);
				if ( c < 'a' || c > 'z') continue;
				freqText[c-'a'] ++;
			}
			
			char[] key = new char[26];
			List<Integer> freqList = Arrays.asList(freqText);
			Character replacementA = replaceA(toDecrypt);
			key[replacementA - 'a'] = 'a';
			freqList.set(replacementA - 'a', -1);
			for (int i = 0 ; i < 26 ; i++){
				if (letters[i] == 'a') continue;
				int max = freqList.indexOf(Collections.max(freqList));
				freqList.set(max, -1);
				key[(char) (max + 'a') - 'a'] = letters[i];
			}	
	    
			for (int i = 0 ; i < toDecrypt.length() ; i++){
				c = toDecrypt.charAt(i);
				if (c < 'a' || c > 'z') continue;
				toDecrypt.setCharAt(i, key[c - 'a']);
			}
		}
		
		
		static int getMax(int[]tab){
			int max=0;
			int index_max=0;
			for(int i = 0; i<tab.length;i++){
				if(tab[i]>max){
					max=tab[i];
					index_max=i;
				}
			}
			tab[index_max] = -1;
			return index_max;
		}
		
		private static String getFreqbigr(StringBuilder toDecrypt){
			String toDecryptStr = toDecrypt.toString();
			Map<String, Double> freq = new HashMap<String, Double>();
			String bigr;
			Double tmp = 0.;
			char c, d;
			for (int i = 0 ; i < toDecryptStr.length() - 1; i++){
				c = toDecryptStr.charAt(i);
				d = toDecryptStr.charAt(i+1);
				if ( c < 'a' || c > 'z' || d < 'a' || d > 'z' ) continue;
				bigr = c + "" + d;
				freq.put(bigr, ((tmp = freq.get(bigr)) == null ? 0 : tmp) + 1 );
			}
			
			
			
			Map<String, Integer> wordsTwoLet = new HashMap<String, Integer>();
			Integer tmp2;
			for ( String word : toDecryptStr.split(" ")){
				if (word.length() == 2){
					wordsTwoLet.put(word, ((tmp2 = wordsTwoLet.get(word)) == null ? 0 : tmp2) + 1 );
				}
			}
			
			int j=0;
			while (!freq.isEmpty()){if(j==2)break;j++;
				String key = Collections.max(freq.entrySet(), Map.Entry.comparingByValue()).getKey();
				for (int i = 0 ; i < bigrams.length ; i++){
					String bi = bigrams[i];
					if (bi != null && (key.charAt(0) == bi.charAt(0) || key.charAt(1) == bi.charAt(1))){
						System.out.println(key+"   "+bi);
						toDecryptStr = toDecryptStr.replace(key.charAt(0), bi.charAt(0));
						toDecryptStr = toDecryptStr.replace(key.charAt(1), bi.charAt(1));
						bigrams[i] = null;
						break;
					}
				}
				//System.out.println(key + " : " + freq.get(key));
				freq.remove(key);
			}
			return toDecryptStr;
		}
	

}

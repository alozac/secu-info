package securiteL3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public interface Chiffrement {
	
	static char[]   letters = {'e', 'a' , 's' , 'i' , 'n'  , 't' , 'r' , 'l' , 'u' , 'o' , 'd' , 'c' , 'p' , 'm' , 'v' , 'g' , 'f' , 'b' , 'q' , 'h' , 'x' , 'j' , 'y' , 'z' , 'k' , 'w'};
	static String[] bigrams = {"es", "de", "le", "en", "re","nt", "on", "er", "te", "el", "an", "se", "et", "la", "ai", "it", "me", "ou", "em", "ie"};
	static char[]   doubles = { 's', 'e', 'l', 't', 'n', 'm', 'r', 'p', 'f', 'c', 'a', 'u', 'i', 'g'};
	
	static HashSet<String> dico = new HashSet<String>();
	
	static void initDico(String fileName) throws IOException{
		BufferedReader bf = new BufferedReader(
								new FileReader(
									new File(fileName)));
		String tmp = "";	
		while(( tmp = bf.readLine()) != null)
			dico.add(StringFormalizer.formalize(tmp).toString());
		bf.close();
	}
	
	
	public void chiffrer(Object obj);

	public void dechiffrer(Object obj);
}
package securiteL3;
import java.io.*;

public class chiffrer {
	public static void main(String[]args){
		if (args.length != 3) {
			System.err.println("Nombre d'arguments insuffisant!");
			return;
		}

		try {
			BufferedReader br = new BufferedReader(
									new FileReader(
										new File(args[2])));
			
			String toCode = "";
			String tmp = "";
			while(( tmp = br.readLine()) != null)
				toCode += tmp + "\n";

			br.close();

			Chiffrement chiffrement = null;
			if(args[0].equals("v")){
				chiffrement = new Vigenere(args[1]);
			} else if(args[0].equals("p")){
				chiffrement = new Permutation(args[1]);
			} else if(args[0].equals("c")){
				chiffrement = new Cesar(args[1]);
			} else chiffrement = null;

			if (chiffrement == null){
				System.err.println("Chiffrement incorrect!");
				return;
			}

			chiffrement.chiffrer(toCode);

		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		} catch (IncorrectKeyException e){
			System.err.println("Clé incorrecte : " + e);
		}
	}
	
}

package securiteL3;

public class Vigenere implements Chiffrement{
	int[] key;
	int key_size;
	
	public Vigenere(String k) throws IncorrectKeyException{
		key = new int[k.length()];
		for (int i = 0 ; i < k.length() ; i++){
			key[i] = k.charAt(i) - 'a' + 1;
		}
		key_size = k.length();
	}
	
	@Override
	public void chiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder s= new StringBuilder();
		int c;
		char tmp;
		for (int i = 0; i < str.length(); i++){
			tmp = str.charAt(i);
			if (tmp >= 'a' && tmp <= 'z'){ 
				c = (tmp + key[i % key_size]);
				if (c > 'z')
					c = c - 'z' + 'a' - 1;
				s.append((char)(c));
			} else s.append(tmp);
		}
		System.out.println("Code : \n"+s);
	}

	@Override
	public void dechiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder s=new StringBuilder();
		int c;
		char tmp;
		for (int i = 0; i < str.length(); i++){
			tmp = str.charAt(i);
			if (tmp >= 'a' && tmp <= 'z'){ 
				c = (tmp - key[i % key_size]);
				if (c < 'a')
					c = c + 'z' - 'a' + 1;
				s.append((char)(c));
			} else s.append(tmp);
		}
		System.out.println("Decode : \n"+s);
	}
	
	

	public static void decrypt(Object obj, int parseInt) {
		// TODO Auto-generated method stub
		
	}

}

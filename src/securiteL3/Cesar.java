package securiteL3;

public class Cesar implements Chiffrement{

	int dist;
	
	public Cesar(String d){
		dist = Integer.parseInt(d);
	}

	@Override
	public void chiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder code = new StringBuilder();
		char c;
		for (int i = 0; i < str.length() ; i++){
			c=(char)(str.charAt(i));
			if ( c < 'a' || c > 'z' ) {
				code.append(c);
				continue;
			}
			c = (char)(c + dist);
			if (c > 'z'){
				c = (char) (c-'z'+'a'-1);
			}
			code.append(c);
		}
		System.out.println(code);
	}

	@Override
	public void dechiffrer(Object obj) {
		StringBuilder str = StringFormalizer.formalize(obj);
		StringBuilder decode= new StringBuilder();
		char c;
		for (int i = 0; i<str.length(); i++){
			c = (char)(str.charAt(i));
			if ( c < 'a' || c > 'z' ){
				decode.append(c);
				continue;
			}
			c = (char)(c - dist);
			if (c<'a'){
				c=(char)(c+'z'-'a'+1);
			}
			decode.append(c);
		}
		System.out.println(decode);
	}

	
	public static void decryptBrute(Object obj) {
		StringBuilder toDecrypt = StringFormalizer.formalize(obj);
		for(int i = 1 ; i<=26; i++){
			if (tryCode(toDecrypt, i))
				break;
		}	
	}

	public static void decryptFreq(Object obj) {
		StringBuilder toDecrypt = StringFormalizer.formalize(obj);
		
		int len = toDecrypt.length();
		int []lettrfreq = new int[26];
		char c = 0;
		for (int i = 0 ; i < len ;i++){
			c = toDecrypt.charAt(i);
			if(c>='a' && c<='z')
				lettrfreq[c - 'a']++;
		}
		
		for (int j = 0 ; j < 26 ; j++){
			int max = getMax(lettrfreq);
			
			int code = (max + 'a') - letters[j];
			if (tryCode(toDecrypt, code))
				return;
		}
	}
	
	private static int getMax(int[]tab){
		int max=0;
		int index_max=0;
		for(int i = 0; i<tab.length;i++){
			if(tab[i]>max){
				max=tab[i];
				index_max=i;
			}
		}
		tab[index_max] = -1;
		return index_max;
	}
	
	

	public static void decryptWord(Object obj, String wd) {
		StringBuilder toDecrypt = StringFormalizer.formalize(obj);
		StringBuilder word = StringFormalizer.formalize(wd);
		
		String []texte = toDecrypt.toString().split("[^a-z]+");
		for(int i = 0 ; i < texte.length ; i++){
			if(texte[i].length() == wd.length()){
				int code =  texte[i].charAt(0) - word.charAt(0);
				String motdecrypt="";
				for(int j = 0; j<wd.length();j++){
					char c;
					c= (char) ((texte[i].charAt(j)) - code);
					if(c<'a') c = (char) (c+'z'-'a'+1);
					if(c>'z') c = (char) (c+'a'-'z'-1);
					motdecrypt+=c;
				}
				if(motdecrypt.equals(word.toString())){
					if(tryCode(toDecrypt, code)) return;
				}
			}
		}
		
	}

	public static boolean tryCode(StringBuilder toDecrypt, int code){
		StringBuilder decrypted = new StringBuilder();
		int len = toDecrypt.length();
		char c = 0;
		String word = "";
		int notInDico = 0;
		for (int k = 0; k < len ; k++){
			c = toDecrypt.charAt(k);
			if(c>='a' && c<='z'){
				c = (char) ((toDecrypt.charAt(k))-code);
				if (c<'a')
					c = (char)(c+'z'-'a'+1);
				if(c>'z')
					c = (char)(c+'a'-'z'-1);
				word += c;
			} else {
				if (!dico.contains(word) && word.length()!=0) notInDico++;
				if(notInDico > 10)
					return false;
				decrypted.append(word + c);
				word = "";
			}
		}
		System.out.println(decrypted);
		return true;		
	}
}

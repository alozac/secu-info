package securiteL3;

public class StringFormalizer {
	
	public static StringBuilder formalize(Object o){
		String str = o.toString();
		StringBuilder form = new StringBuilder();
		Character c;
		for (int i = 0 ; i < str.length() ; i++){
			c = Character.toLowerCase(str.charAt(i));
			Character.toLowerCase(c);
			if (c < 'a' || c > 'z')
				form.append(removeAccents(c));
			else form.append(c);
		}
		return form;
	}

	private static char removeAccents(Character c) {
		switch(c){
		case '�':
		case '�':
		case '�':
		case '�':
			return 'e';
		case '�':
		case '�':
		case '�':
			return 'a';
		case '�':
			return 'o';
		case '�':
		case '�':
			return 'i';
		case '�':
		case '�':
		case '�':
			return 'u';
		case '�':
			return 'c';
		default:
			return c;
		}
	}
}